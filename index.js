// Require the necessary classes
const fs = require('fs');
const { Client, Intents } = require('discord.js');
const Discord = require('discord.js');
const { clientId, token, Wtoken } = require('./config.json');
const axios = require("axios");
const {Wit, log} = require('node-wit');
const get_temperature = require('./intents/get_temperature');
const rep_nom = require('./intents/rep_nom');
const rep_age = require('./intents/rep_age');
const date = require('./intents/date');
const rep_presentation = require('./intents/rep_presentation');
const traduction = require('./intents/traduction');
const witClient = new Wit({
  accessToken: Wtoken,
  logger: new log.Logger(log.DEBUG) // optional
});

// Create a new client instance
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });

//TEST DU BOT DISCORD 

/*async function getUserBannerUrl(userId, { dynamicFormat = true, defaultFormat = "webp", size = 512 } = {}) {

    // Supported image sizes, inspired by 'https://discord.js.org/#/docs/main/stable/typedef/ImageURLOptions'.
    if (![16, 32, 64, 128, 256, 512, 1024, 2048, 4096].includes(size)) {
        throw new Error(`The size '${size}' is not supported!`);
    }

    // We don't support gif as a default format,
    // because requesting .gif format when the original image is not a gif,
    // would result in an error 415 Unsupported Media Type.
    // If you want gif support, enable dynamicFormat, .gif will be used when is it available.
    if (!["webp", "png", "jpg", "jpeg"].includes(defaultFormat)) {
        throw new Error(`The format '${defaultFormat}' is not supported as a default format!`);
    }

    // We use raw API request to get the User object from Discord API,
    // since the discord.js v12's one doens't support .banner property.
    const user = await client.api.users(userId).get();
    if (!user.banner) return null;

    const query = `?size=${size}`;
    const baseUrl = `https://cdn.discordapp.com/banners/${userId}/${user.banner}`;

    // If dynamic format is enabled we perform a HTTP HEAD request,
    // so we can use the content-type header to determine,
    // if the image is a gif or not.
    if (dynamicFormat) {
        const { headers } = await axios.head(baseUrl);
        if (headers && headers.hasOwnProperty("content-type")) {
            return baseUrl + (headers["content-type"] == "image/gif" ? ".gif" : `.${defaultFormat}`) + query;
        }
    }

    return baseUrl + `.${defaultFormat}` + query;
}

// When the client is ready, run this code (only once)
client.once('ready', () => {
	console.log('Ready!');
});  
client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

	const { commandName } = interaction;
    var date = new Date(interaction.user.createdTimestamp)
    var heures = date.getHours();
    var jour = date.getUTCDate();
    var mois = date.getUTCMonth();
    var année = date.getFullYear().toString();
    var tab_mois=new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    const bannerUrl = await getUserBannerUrl(interaction.user.id, { size: 4096 });

	if (commandName === 'ping') {
		await interaction.reply('Pong!');
	} else if (commandName === 'server') {
		await interaction.reply(`Server name: ${interaction.guild.name}\nTotal members: ${interaction.guild.memberCount}\nAvatar: \n${interaction.guild.iconURL()}`);
	} else if (commandName === 'user') {
		await interaction.reply(`Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}\nLe compte a été créé le ${jour} ${tab_mois[mois]} ${année} à ${heures}h`);
	} else if (commandName === 'avatar') {
		await interaction.reply(`Ton avatar: \n${interaction.user.displayAvatarURL()}`);
	} else if (commandName === 'banner') {
		await interaction.reply(`Ta bannière: \n${bannerUrl}`);
	} else if (commandName === 'question') {
		await interaction.reply("Quel est ton nom?");
        //const collector = new Discord.MessageCollector(interaction.channel, m => m.author.id === interaction.author.id, { time: 10000 });
	}
});*/

// for Wit intents
let intents = new Discord.Collection();
const intentFiles = fs.readdirSync('./intents').filter(file => file.endsWith('.js'));

//Pour chaque ficher js dans ./intents
for (const file of intentFiles) {
    const intent = require(`./intents/${file}`);
    intents.set(intent.name, intent);
}

/**
 * Démarrage du bot
 */
client.on('ready', () => {
    console.log('Conversation ready to receive');
});
//Lorsqu'un message est envoyé d'un utilisateur, traitement du message
client.on('messageCreate', message => {

    if (message.author.bot) {
        // permet de ne pas traiter les messages qu'envoie le bot
        return;
    }
    witClient.message(message, {})
    .then((data) => {
        //console.log(data['intents']);
        console.log(data['intents'][0]['name']);
        //Je vérifie si il a bien trouvé un intent dans mon message
        if(data['intents'][0]['name'] != undefined) {
            //console.log(data['intents'][0]['name']);
            const intent = intents.get(data['intents'][0]['name']);
            //console.log(intent);
            args = {}; // context variables here?

            if (!intent) {
                return message.reply(`Je ne comprends pas.`);
            }
            else if(intent === get_temperature){ //Je vérifie les intents en fonction des fichiers
                let ville = data['entities']['wit$location:location'][0]['value'].toLowerCase();
                //console.log(ville);
                intent.getWeather(message, ville);
            }
            else if(intent === rep_nom){
                let nom = data['entities']['wit$contact:contact'][0]['value'];
                //console.log(nom);
                intent.execute(message, nom);
            }
            else if(intent === rep_age){
                let age = data['entities']['wit$age_of_person:age_of_person'][0]['value'];
                console.log(age);
                intent.execute(message, age);
            }
            else if(intent === date){
                let date = data['entities']['wit$datetime:datetime'][0]['value'];
                console.log(date);
                intent.execute(message, date);
            }
            else if(intent === rep_presentation){
                let nom = data['entities']['wit$contact:contact'][0]['value'];
                let age = data['entities']['wit$age_of_person:age_of_person'][0]['value'];
                intent.execute(message, nom, age);
            }
            /*else if(intent === traduction){
                let date = data['entities']['wit$datetime:datetime'][0]['value'];
                console.log(date);
                intent.execute(message, date);
            }*/
            else{
                try {
                    //Si rien aucune paramètre n'est précisé alors il execute la fonction telle quelle
                    intent.execute(message);
                }
                catch (error) {
                    console.error(error);
                    message.reply(`Erreur, je ne peux pas faire ça.`);
                }
            }
        }
    })
    .catch(console.error);
});

// Login to Discord with your client's token
client.login(token);