module.exports = {
    name: 'get_temperature', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Demande la temperature',

    getWeather: function (message, ville) {
            let { OpenWeatherMap } = require('../openweather/open.js');
            let cle = require("../openweather/cle.js");
            let ow = new OpenWeatherMap(cle.cle, 'metric');
            ow.getWeather(ville+',fr').then(data => {
                message.channel.send('La temperature est de : '+data.main.temp);
                //console.log('La temperature est de :', data.main.temp);
            }).catch(err => {
                message.channel.send('Erreur API');
            });
        }
};