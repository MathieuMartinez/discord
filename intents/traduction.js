module.exports = {
    name: 'traduction', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Demande de la traduction',

    get_trad(phrase){
        var axios = require("axios").default;
        var options = {
        method: 'POST',
        url: 'https://deep-translate1.p.rapidapi.com/language/translate/v2',
        headers: {
            'content-type': 'application/json',
            'x-rapidapi-host': 'deep-translate1.p.rapidapi.com',
            'x-rapidapi-key': '2f77d66fc0msh327054760138e82p1637aejsn26b3f7f80f12'
        },
        data: {q: phrase, source: 'fr', target: 'en'}
        };
        axios.request(options).then(function (response) {
            message.channel.send('La traduction est : '+response.data);
        }).catch(function (error) {
            console.error(error);
        });
    }
};