module.exports = {
    name: 'rep_presentation', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Réponse suite à la présentation',
    execute(message, nom, age) {
        message.channel.send('Enchanté '+nom+' !');
        message.channel.send('Tu as '+age+" ! moi j'ai seulement deux semaines :grin:");
        message.channel.send("Ou habites tu ?");
    },
};