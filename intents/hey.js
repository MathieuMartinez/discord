module.exports = {
    name: 'hey', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Salutations',
    execute(message) {
        message.channel.send(`Salut ! Comment vas-tu ?`);
    },
};