module.exports = {
    name: 'date', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Demande la date',
    execute(message, date) {
        var date = new Date(date);
        var jour = date.getUTCDate();
        var mois = date.getUTCMonth();
        var année = date.getFullYear().toString();
        var tab_mois=new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

        message.channel.send('Le '+(jour+1)+' '+tab_mois[mois]+' '+année);
    },
};