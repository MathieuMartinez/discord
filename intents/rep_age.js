module.exports = {
    name: 'rep_age', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Réponse suite à son age',
    execute(message, age) {
        message.channel.send(age+" ! moi j'ai deux semaines :grin:");
        message.channel.send("Ou habites tu ?");
    },
};