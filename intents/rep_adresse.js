module.exports = {
    name: 'rep_adresse', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Reponse suite à son adresse',
    execute(message) {
        message.channel.send("Ok ! ça m'a l'air d'être une belle ville");
    },
};