module.exports = {
    name: 'rep_nom', // needs to match both the filename and the intent-name at Wit.ai
    description: 'Réponse suite au nom',
    execute(message, nom) {
        message.channel.send('Enchanté '+nom+' quel âge as tu ?');
    },
};